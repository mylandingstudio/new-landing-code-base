let R;

export default class LayoutManager {
        
    static get BASIC_WIDTH_INTERVAL() {return 320; }
    static get NARROW_LAYOUT_MAX_WIDTH() { return  LayoutManager.BASIC_WIDTH_INTERVAL*2 -1 ; }
    
    static get VIEW_MODE_ON_NARROW_LAYOUT() { return {
            NAV_MENU: 0, MAIN_CONTENT: 1
    }}

    static get COUNTDOWN_BEFORE_RELAYOUT_AFTER_WINDOW_WIDTH_HAS_BEEN_CHANGED(){ 
            return 0; }
        
        
    constructor(Resources){
        
        R = Resources;
        this.$DocumentWindow = $(window);
        this.currentWindowWidth;
        this.currentViewModeOnNarrowLayout = LayoutManager.VIEW_MODE_ON_NARROW_LAYOUT.NAV_MENU;
        
        this.$NavPanel = $(R.ccs.NavPanel.RootElement);
        this.$MainContent = $(R.ccs.Layout.MainContent);
        
        this.setupRelayoutWhenViewportHasBeenResized();
    }
    
    
    setupLayout(){
        
        this.currentWindowWidth = this.$DocumentWindow.width();
        
        if (this.currentWindowWidth > LayoutManager.NARROW_LAYOUT_MAX_WIDTH) {
            this.displayBothNavPanelAndMainContent();
        }
        else {
            switch (this.currentViewModeOnNarrowLayout){
                case LayoutManager.VIEW_MODE_ON_NARROW_LAYOUT.NAV_MENU:
                    this.displayNavPanelOnly();
                    break;
                case LayoutManager.VIEW_MODE_ON_NARROW_LAYOUT.MAIN_CONTENT:
                    this.displayMainContentOnly();
                    break;
            }
        }
    }
    
    displayBothNavPanelAndMainContent(){
        
        this.currentViewModeOnNarrowLayout = LayoutManager.VIEW_MODE_ON_NARROW_LAYOUT.MAIN_CONTENT;
        
        this.$NavPanel
                .show()
                .removeClass(R.cc.NavPanel.WidthSetupModifiers.fillParentWidth)
                .addClass(R.cc.NavPanel.WidthSetupModifiers.fixWidth);
        
        this.$MainContent.show();
    }
    
    displayNavPanelOnly(){
        
        this.$NavPanel
                .show()
                .removeClass(R.cc.NavPanel.WidthSetupModifiers.fixWidth)
                .addClass(R.cc.NavPanel.WidthSetupModifiers.fillParentWidth);
        
        this.$MainContent.hide();
    }
    
    displayMainContentOnly(){
        
        this.currentViewModeOnNarrowLayout = LayoutManager.VIEW_MODE_ON_NARROW_LAYOUT.MAIN_CONTENT;
        
        this.$NavPanel.hide();
        this.$MainContent.show();
    }
    
    setupRelayoutWhenViewportHasBeenResized(){

        let countdownAfterWindowWidthHasBeenChanged;

        this.$DocumentWindow.on('resize', () => {

            stopPreviousCountdown();

            countdownAfterWindowWidthHasBeenChanged = setTimeout( ()=> {
                this.setupLayout();
            }, LayoutManager.COUNTDOWN_BEFORE_RELAYOUT_AFTER_WINDOW_WIDTH_HAS_BEEN_CHANGED)
        });

        function stopPreviousCountdown() {
            clearTimeout(countdownAfterWindowWidthHasBeenChanged);
        }
    }
}