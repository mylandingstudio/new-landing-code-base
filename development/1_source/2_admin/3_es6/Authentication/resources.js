import Resources from  'FrameworkRoot/Resources';

export default class AuthenticationResources extends Resources {
        
    constructor() {
        
        const CSS_CLASSES = {
            
            SignInForm: {
                InputFields: {
                    common: 'Authentication-InputField',
                    statusModifiers: {
                        invalid: 'invalid'
                    }
                }
            }
        };
        
        const ID = {

            SignInForm: {
                HtmlForm: 'SignInForm-HtmlForm',
                InputFields: {
                    Email: 'Email-InputField-SignIn',
                    Password: 'Password-InputField-SignIn'
                },
                ErrorMessages: {
                    EmptyEmail: 'EmptyEmail-ErrorMessage',
                    InvalidEmail: 'InvalidEmail-ErrorMessage',
                    EmptyPassword: 'PasswordIsEmpty-ErrorMessage'
                },
                Buttons: {
                    SignIn: 'SignIn-Button'
                }
            }
        }
        
        const STRINGS = {
            
            SignInForm: {
                Messages: {
                    SigningIn: 'Выполняется вход ...'
                }
            }
        };
        
        super(CSS_CLASSES, ID, STRINGS);
        
        this.routes = {
            login: {
                development: '00-01_SignIn',
                production: 'login'
            }
        }
    }
}