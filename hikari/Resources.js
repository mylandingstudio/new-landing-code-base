export default class Resources{
        
    constructor(CSS_CLASSES, ID, STRINGS){
        
        this.cc = CSS_CLASSES;
        this.id = ID;
        this.ccs = Resources.cs2ccs(CSS_CLASSES);
        this.ids = Resources.id2ids(ID);
        
        this.s = STRINGS;
        
    }
        
    static get ht() { }
        
    
    static cs2ccs(CSS_CLASSES) {
            
        let outputObjectWorkpiece = $.extend(true, {}, CSS_CLASSES);
            
        function cs2css(outputObjectWorkpiece){
            for (let key in outputObjectWorkpiece){
                
                if (outputObjectWorkpiece.hasOwnProperty(key)) {
                    
                    if (typeof(outputObjectWorkpiece[key]) === 'string') {
                        outputObjectWorkpiece[key] = '.' + outputObjectWorkpiece[key];
                    }
                    else if (typeof(outputObjectWorkpiece[key]) === 'object'){
                        cs2css(outputObjectWorkpiece[key]);
                    }
                }
            }
            
            return outputObjectWorkpiece;
        }
        
        return cs2css(outputObjectWorkpiece);

    }


    static id2ids(ID) {
            
        let outputObjectWorkpiece = $.extend(true, {}, ID);
            
        function id2ids(outputObjectWorkpiece){
            for (let key in outputObjectWorkpiece){
                
                if (outputObjectWorkpiece.hasOwnProperty(key)) {
                    
                    if (typeof(outputObjectWorkpiece[key]) === 'string') {
                        outputObjectWorkpiece[key] = '#' + outputObjectWorkpiece[key];
                    }
                    else if (typeof(outputObjectWorkpiece[key]) === 'object'){
                        id2ids(outputObjectWorkpiece[key]);
                    }
                }
            }
            return outputObjectWorkpiece;
        }
        
        return id2ids(outputObjectWorkpiece);
    };
    
    
    createCompoundClassSelector(...classNames){
        
        let selectorWillBeReturned = '';
        
        classNames.forEach( className => {
            selectorWillBeReturned = selectorWillBeReturned + '.' + className;
        })
        return selectorWillBeReturned;
    };
    
    createCommaSeparatedCompoundSelector(...selectors){
        
        let selectorWillBeReturned = '';
        let firstIteration = true;
        
        selectors.forEach( selector => {
            
            if (firstIteration) {
                selectorWillBeReturned = selectorWillBeReturned + selector;
                firstIteration = false;
            }
            else {
                selectorWillBeReturned = selectorWillBeReturned + ',' + selector;
            }
        })
        return selectorWillBeReturned;
    }
}