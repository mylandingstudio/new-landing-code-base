export default class HString {
        
    static replaceNewLineEscapeSequenceToHtmlBr(string){
        
        if (typeOf(string) !== 'string') {
            throw new Error(`HString.replaceNewLineEscapeSequenceToHtmlBr　のエラー:　パラメータの型は「行」だと待機されたが、
                    「${typeOf(string)}」に成り、値：${string}である。`)
        }
        
        return string.replace(/\r?\n/g, '<br />');
    }
    
    static replaceHtmlBrToNewLineEscapeSequence(string){
        
        if (typeOf(string) !== 'string') {
            throw new Error(`HString.replaceHtmlBrToNewLineEscapeSequence　のエラー:　パラメータの型は「行」だと待機されたが、
                    「${typeOf(string)}」に成り、値：${string}である。`)
        }
        
        return string.replace(/<br\s*\/*>/g, '\n');
    }
    
            
}