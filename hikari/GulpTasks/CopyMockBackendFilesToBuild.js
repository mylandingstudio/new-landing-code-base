'use strict';

const   gulp = require('gulp'),
            plugins = require('gulp-load-plugins')();

module.exports = options => {
    
    return () => {

        return gulp.src(options.HPath.mockBackendPhpFilesSelection)
            .pipe(gulp.dest( file => {

                let fileBase = file.base;
        
                if (fileBase.indexOf(options.HPath.folderNames.development.source.open.root) !== -1) {
                    return options.HPath.mockBackendFilesOutputPaths.developmentBuild.open;
                }
                else if (fileBase.indexOf(options.HPath.folderNames.development.source.admin.root) !== -1) {
                    return options.HPath.mockBackendFilesOutputPaths.developmentBuild.admin;
                }
                else {
                    throw new Error('Copy Mock Backend Files To Build 課題、gulp.destでの例外：源パス「' + fileBase + '」に該当する出力パスが定義されていない。');
                }
            }));
    };
};