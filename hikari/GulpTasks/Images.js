'use strict';

const   gulp = require('gulp'),
            gulpIf = require('gulp-if'),
            fs = require('fs'),
            plugins = require('gulp-load-plugins')(),
            
            pngquant     = require('imagemin-pngquant');

module.exports = options => {
    
    return () => {
        return gulp.src(options.HPath.imagesSourceFilesSelection,
        {since: options.isDevelopment ? gulp.lastRun('images') : null })
                .pipe(gulpIf(options.isDevelopment, plugins.newer(options.HPath.imagesOutputPaths.developmentBuild.common)))
                .pipe(gulpIf(options.isDevelopment, plugins.newer(options.HPath.imagesOutputPaths.developmentBuild.open)))
                .pipe(gulpIf(options.isDevelopment, plugins.newer(options.HPath.imagesOutputPaths.developmentBuild.admin)))
                .pipe(gulpIf(options.isDevelopment, plugins.newer(options.HPath.imagesOutputPaths.developmentBuild.development)))
                .pipe(gulpIf(!options.isDevelopment, plugins.cache(
                    plugins.imagemin({ 
                    interlaced: true,
                    progressive: true,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [pngquant()]
                    })
                )))
                .pipe(plugins.debug({title: 'images'}))
                .pipe(gulp.dest( file => {

                    let fileBase = file.base;
        
                    if (fileBase.indexOf(options.HPath.folderNames.development.source.common.root) !== -1) {
                        if (options.isDevelopment) {
                            return options.HPath.imagesOutputPaths.developmentBuild.common;
                        }
                        return options.HPath.imagesOutputPaths.productionBuild.common;
                    }
                    if (fileBase.indexOf(options.HPath.folderNames.development.source.open.root) !== -1) {
                        if (options.isDevelopment) {
                            return options.HPath.imagesOutputPaths.developmentBuild.open;
                        }
                        return options.HPath.imagesOutputPaths.productionBuild.open;
                    }
                    else if (fileBase.indexOf(options.HPath.folderNames.development.source.admin.root) !== -1) {
                        if (options.isDevelopment) {
                            return options.HPath.imagesOutputPaths.developmentBuild.admin;
                        }
                        return options.HPath.imagesOutputPaths.productionBuild.admin;
                    }
                    // 納品版の時は此方には来ない様に設定しなければならない。
                    else if (fileBase.indexOf(options.HPath.folderNames.development.source.development.root) !== -1) {
                        if (options.isDevelopment) {
                            return options.HPath.imagesOutputPaths.developmentBuild.development;
                        }
                        return options.HPath.imagesOutputPaths.productionBuild.development;
                    }
                    else {
                        throw new Error('Styles 課題、gulp.destでの例外：源パス「' + fileBase + '」に該当する出力パスが定義されていない。');
                    }
            }));
    };
};