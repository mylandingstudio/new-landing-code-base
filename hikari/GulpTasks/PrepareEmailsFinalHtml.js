'use strict';

const   gulp = require('gulp'),
            plugins = require('gulp-load-plugins')();

module.exports = options => {
    
    return () => {
        
        return gulp.src(options.HPath.emailInterimHtmlFilesSelction)
        
            .pipe(plugins.plumber({
                errorHandler: plugins.notify.onError( error => {
                    return {
                        title: 'Prepare Final Emails HTML:',
                        message: error.message
                    };
                })
            }))
            
            .pipe(plugins.emailBuilder().inlineCss())
            .pipe(plugins.htmlPrettify({indent_char: ' ', indent_size: 4}))
    
            .pipe(gulp.dest( file => {
                return options.HPath.emailsHtmlFilesOutput;
            }));
    };
};