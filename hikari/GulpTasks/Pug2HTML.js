'use strict';

const   gulp = require('gulp'),
            plugins = require('gulp-load-plugins')();

module.exports = options => {
    
    return () => {
        
        return gulp.src(options.HPath.pugSourceFilesSelection.development)
    
            .pipe(plugins.plumber({
                errorHandler: plugins.notify.onError( error => {
                    return {
                        title: 'Pug To HTML:',
                        message: error.message
                    };
                })
            }))
        
            .pipe(plugins.pug())
            .pipe(plugins.htmlPrettify({indent_char: ' ', indent_size: 4}))
    
            .pipe(gulp.dest( file => {

                let fileBase = file.base;

                if (fileBase.indexOf(options.HPath.folderNames.development.source.open.root) !== -1) {
                    return options.HPath.htmlOutputPaths.developmentBuild.open;
                }
                else if (fileBase.indexOf(options.HPath.folderNames.development.source.admin.root) !== -1) {
                    return options.HPath.htmlOutputPaths.developmentBuild.admin;
                }
                else if (fileBase.indexOf(options.HPath.folderNames.development.source.development.root) !== -1) {
                    
                    if (file.stem === 'index') {
                        return options.HPath.htmlOutputPaths.developmentBuild.index;
                    }

                    return options.HPath.htmlOutputPaths.developmentBuild.development;
                }
                else {
                    throw new Error('pug to html 課題、gulp.destでの例外：源パス「' + fileBase + '」に該当する出力パスが定義されていない。');
                }
            }));
    };
};