'use strict';

const   gulp = require('gulp'),
            plugins = require('gulp-load-plugins')();

module.exports = options => {
    
    return () => {
        return gulp.src(options.HPath.frameworkSassFilesSelection)
            .pipe(plugins.newer(options.HPath.frameworkSassFilesDestination.open))
            .pipe(plugins.newer(options.HPath.frameworkSassFilesDestination.admin))
            .pipe(plugins.newer(options.HPath.frameworkSassFilesDestination.development))
            .pipe(plugins.debug({title: 'provide framework style dependences'}))
            .pipe(gulp.dest(options.HPath.frameworkSassFilesDestination.open))
            .pipe(gulp.dest(options.HPath.frameworkSassFilesDestination.admin))
            .pipe(gulp.dest(options.HPath.frameworkSassFilesDestination.development));
    };
};