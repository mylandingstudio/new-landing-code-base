@extends('open/layouts/basic')

@section('CssLinks')
    <link href="{{ asset('open/css/01_Top.css') }}" rel="stylesheet">
@endsection


@section('Content')

    <?php /*
    * Это сообщения, которые генерируются сервером при отправке невалидных данных. 
    * В идеале, засчёт валидации данных средствами HTML и JavaScript эти сообщения никогда 
    * не должны отобразиться, тем менее проверка данных на стороне сервера необходима 
    * (опять же, она уже реализована, Вам же осталось только сверстать сообщения об ошибке).
    */?>

    @if(count($errors) > 0)
        <div class="FeedbackForm-ServerErrors-RootWrapper">
            <div class="FeedbackForm-ServerErrors-Centerer">
                <div class="FeedbackForm-ServerErrors-Title">
                    При отправке формы заявки обнаружены следующие ошибки:
                </div>
                <ul class="FeedbackForm-ServerErrors-ErrorsList">
                    @foreach($errors -> all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <div>Главная страница</div>
    <?php /*
    * Вставьте сюда основное содержимое страницы
    */?>
    
@endsection

@section('HiddenElements')

    <?php /*
    * Сюда можно подключить скрытые абсолютно позиционируемые элементы, такие как модальные окна, 
     * в том числе окно с формой обратной связи.
    */?>

    @include('open/widgets/FeedbackForm')
@endsection

@section('EndBodyJS')
    <script src="{{ asset('open/js/Top.js') }}"></script>
@endsection