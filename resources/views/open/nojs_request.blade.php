@extends('open/layouts/basic')

@section('CssLinks')
    <link href="{{ asset('open/css/01-01_NoJsRequest.css') }}" rel="stylesheet">
@endsection

@section('Content')

@if(count($errors) > 0)
    <div class="FeedbackForm-ServerErrors-RootWrapper">
        <div class="FeedbackForm-ServerErrors-Centerer">
            <div class="FeedbackForm-ServerErrors-Title">
                При отправке формы заявки обнаружены следующие ошибки:
            </div>
            <ul class="FeedbackForm-ServerErrors-ErrorsList">
                @foreach($errors -> all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

@endsection

@section('HiddenElements')
    @include('open/widgets/FeedbackForm')
@endsection

@section('EndBodyJS')
    <script src="{{ asset('open/js/NoJsRequest.js') }}"></script>
@endsection