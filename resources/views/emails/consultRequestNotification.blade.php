<div class="Email-ContainsWrapper" style="font-family: Verdana,Geneva,sans-serif; font-size: 13px;">
    <div class="Email-ParagraphsBlock">
        <p>Поступил новый запрос на консультацию:</p>
    </div>
    <ul class="Email-RequestDataList">
        <li> 
            <span class="Email-RequestDataList-Title" style="margin-right: 4px;">Время запроса (Москва):</span>
            <span class="Email-RequestDataList-Description" style="font-weight: 700; color: #4183d7;">{{ $feedackSentTime }}</span>
        </li>
        <li> 
            <span class="Email-RequestDataList-Title" style="margin-right: 4px;">Указанное имя:</span>
            <span class="Email-RequestDataList-Description" style="font-weight: 700; color: #4183d7;">{{ $inputedData[$inputFieldNames['name']] }}</span>
        </li>
        <li> 
            <span class="Email-RequestDataList-Title" style="margin-right: 4px;">Указанный email:</span>
            <span class="Email-RequestDataList-Description" style="font-weight: 700; color: #4183d7;">{{ $inputedData[$inputFieldNames['email']] }}</span>
        </li>
    </ul>
    <div class="Email-ParagraphsBlock">
        @switch($activeFirstReplyMethod)
            @case('FullAutoReply')
                <p>Поскольку у Вас настроен <span class="Email-Typo-Accent" style="font-weight: 700; color: #d64541;">полностью автоматический ответ</span>, то письмо потенциальному клиенту будет отправлено <span class="Email-Typo-Accent" style="font-weight: 700; color: #d64541;">автоматически </span>через настроенный Вами промежуток времени.</p>
                @break
            @case('FullAutoReplyAfterWaiting')
                <p>У Вас настроен <span class="Email-Typo-Accent" style="font-weight: 700; color: #d64541;">полуавтоматический ответ </span>с <span class="Email-Typo-Accent" style="font-weight: 700; color: #d64541;">автоматической отправкой письма по истечении указанного в настройках времени</span>, а потому во избежание плохого впечатления со стороны клиента из-за вероятной некорректной подстановки в письма указанного им имени следует зайти в админ-панель и выбрать вариант письма в качестве ответа на заявку.</p>
                @break
            @case('SemiAutoReply')
                <p>У Вас настроен <span class="Email-Typo-Accent" style="font-weight: 700; color: #d64541;"> полуавтоматический ответ без автоматической отправки письма по истечении указанного в настройках времени</span>, а потому следует зайти в админ-панель и выбрать вариант письма в качестве ответа на заявку. Без этого, письма потенциальному клиенту отправлено не будет.</p>
                @break
            @case('FullManualReply')
                <p>У Вас настроен <span class="Email-Typo-Accent" style="font-weight: 700; color: #d64541;">полностью ручной режим ответа</span>, потому от Вас требуется <span class="Email-Typo-Accent" style="font-weight: 700; color: #d64541;">самостоятельно</span>написать и отправить письмо потенциальному клиенту (нажатие кнопки «ответ» автоматически подставит электронный адрес потенциального клиента в поле получателя).</p>
        @endswitch
    </div>
    <div class="Email-ToAdminPanelLink-Wrapper"><a class="Email-Typo-Link" href="#" style="color: #4183d7; border-bottom: 1px solid #4183d7; padding-bottom: 1px; text-decoration: none;">Доступ к админ-панели</a>
    </div>
</div>