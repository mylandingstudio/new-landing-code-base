<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Extenders\References as REF;


class CreateLandingVisitsTable extends Migration {
    
    public function up() {

        Schema::create(REF::DB_TABLES['landingVisits']['tableName'], function (Blueprint $table) {
            
            $table -> increments(REF::DB_TABLES['landingVisits']['fieldNames']['id']);

            // https://stackoverflow.com/a/1076755/4818123
            $table -> string(REF::DB_TABLES['landingVisits']['fieldNames']['ipAddress'], 45) -> nullable();
            $table -> timestampTz(REF::DB_TABLES['landingVisits']['fieldNames']['visitDate']) -> nullable();
            $table -> text(REF::DB_TABLES['landingVisits']['fieldNames']['sourceReference']) -> nullable();
            $table -> integer(REF::DB_TABLES['landingVisits']['fieldNames']['splitTestId']) -> nullable();

            $table -> string(REF::DB_TABLES['landingVisits']['fieldNames']['utmSource']) -> nullable();
            $table -> string(REF::DB_TABLES['landingVisits']['fieldNames']['utmCampaign']) -> nullable();
            $table -> string(REF::DB_TABLES['landingVisits']['fieldNames']['utmContent']) -> nullable();
            $table -> string(REF::DB_TABLES['landingVisits']['fieldNames']['utmTerm']) -> nullable();
        });
    }

    public function down() {
        Schema::dropIfExists(REF::DB_TABLES['landingVisits']['tableName']);
    }
}