<?php

namespace App\Http\Controllers\Open;
use App\Http\Controllers\Controller;

use DB;
use Carbon\Carbon;

use App\Extenders\References as REF;
use App\Extenders\Notifier;

class TopPageController extends Controller {
    
    private $visitData = array();
    
    private static $actualCarbonTimezone = 'Europe/Moscow';
    private static $carbonTimeFormate = 'Y-m-d H:i:s';
    
    private static $PATH_TO_SPLIT_TEST_DATA_FILE;
    private $splitTestFullData;  
    private $splitTestIdArray;
    private $splitTestDataSubarray;

    
    public function __construct() {
        
        self::$PATH_TO_SPLIT_TEST_DATA_FILE = storage_path().REF::SUBPATHS['storage']['splitTestData'];
        $this -> splitTestFullData = json_decode(file_get_contents(self::$PATH_TO_SPLIT_TEST_DATA_FILE), true);
        $this -> splitTestIdArray = $this -> splitTestFullData['Id'];
        $this -> splitTestDataSubarray = $this -> splitTestFullData['Data'];
    }

    public function renderTopPage(){
        
        $this -> manageLandingVisitData();
        
        return view(REF::OPEN_VIEWS['top']) -> with([
            'pricePlanSplitTestData' => $this -> getSplitTestTestDataForView()]);
    }
    
    private function manageLandingVisitData(){
        
        if (!$this -> hasSessionAlreadyHasVisitId()) {
            
//            dump('Сессия не имеет VisitID; сгенерированы новые данные и добавлены в БД');
            $this -> generateNewVisitData();
            $this -> addVisitDataToDatabase();
            $this -> addVisitDataToSession();
            return;
        }
        
        $this -> visitData[REF::SESSION_VARIABLES['visitId']] = 
                    request() -> session() -> get(REF::SESSION_VARIABLES['visitId']);
        
        if (!$this -> hasDatabaseVisitRecordWithVisiIdFromSession()) {
            
            Notifier::notifyThatVisitIdExistsInSessionButDoesNotExistInDatabase();
            
            $this -> generateNewVisitData();
            $this -> addVisitDataToDatabase();
            $this -> addVisitDataToSession();
            return;
        }
        
        $this -> generateVisitDataForChecking();
        
        if (!$this -> areCurrentIpAddressAndUtmDataMatchesWidthVisitRecordInDb()) {
            Notifier::notifyThatUrlParametersChangedDuringSameVisit();
        }
        
        $this -> getVisitDataFromDatabase();
    }
    
    private function generateNewVisitData() {
        
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['ipAddress']] = request() -> ip();
        
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['visitDate']] = 
                Carbon::now(self::$actualCarbonTimezone) -> format(self::$carbonTimeFormate);
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['sourceReference']] = 
                request() -> server('HTTP_REFERER');
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['splitTestId']] = $this -> generateSplitTestId();
        
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmSource']] = 
                request() -> has(REF::GET_PARAMETERS['utmSource']) ? request() -> {REF::GET_PARAMETERS['utmSource']} : null;
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmCampaign']] = 
                request() -> has(REF::GET_PARAMETERS['utmCampaign']) ? request() -> {REF::GET_PARAMETERS['utmCampaign']} : null;
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmContent']] = 
                request() -> has(REF::GET_PARAMETERS['utmContent']) ? request() -> {REF::GET_PARAMETERS['utmContent']} : null;
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmTerm']] = 
                request() -> has(REF::GET_PARAMETERS['utmTerm']) ? request() -> {REF::GET_PARAMETERS['utmTerm']} : null;
    }
    
    private function generateVisitDataForChecking(){
        
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['ipAddress']] = request() -> ip();
        
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmSource']] = 
                request() -> has(REF::GET_PARAMETERS['utmSource']) ? request() -> {REF::GET_PARAMETERS['utmSource']} : null;
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmCampaign']] = 
                request() -> has(REF::GET_PARAMETERS['utmCampaign']) ? request() -> {REF::GET_PARAMETERS['utmCampaign']} : null;
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmContent']] = 
                request() -> has(REF::GET_PARAMETERS['utmContent']) ? request() -> {REF::GET_PARAMETERS['utmContent']} : null;
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmTerm']] = 
                request() -> has(REF::GET_PARAMETERS['utmTerm']) ? request() -> {REF::GET_PARAMETERS['utmTerm']} : null;
    }
    
    private function addVisitDataToDatabase() {
        
        $this -> visitData[REF::SESSION_VARIABLES['visitId']] = 
                DB::table(REF::DB_TABLES['landingVisits']['tableName']) -> insertGetId([
                    
                        REF::DB_TABLES['landingVisits']['fieldNames']['ipAddress'] => 
                            $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['ipAddress']],
                    
                        REF::DB_TABLES['landingVisits']['fieldNames']['visitDate'] => 
                            $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['visitDate']],
                    
                        REF::DB_TABLES['landingVisits']['fieldNames']['sourceReference'] => 
                            $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['sourceReference']],
                    
                        REF::DB_TABLES['landingVisits']['fieldNames']['splitTestId'] => 
                            $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['splitTestId']],
                    
                        REF::DB_TABLES['landingVisits']['fieldNames']['utmSource'] => 
                            $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmSource']],
                    
                        REF::DB_TABLES['landingVisits']['fieldNames']['utmCampaign'] => 
                            $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmCampaign']],
                    
                        REF::DB_TABLES['landingVisits']['fieldNames']['utmContent'] => 
                            $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmContent']],
                    
                        REF::DB_TABLES['landingVisits']['fieldNames']['utmTerm'] => 
                            $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmTerm']]
            ]);
    }
    
    private function addVisitDataToSession() {
        request() -> session() 
                -> put(REF::SESSION_VARIABLES['visitId'], $this -> visitData[REF::SESSION_VARIABLES['visitId']]);
    }
    
    private function generateSplitTestId(){
        
        $splitTestData = json_decode(file_get_contents(self::$PATH_TO_SPLIT_TEST_DATA_FILE), true); 
        $splitTestIdSubArray = $splitTestData['Id'];
        $splitTestIdCount = count($splitTestIdSubArray);
        return mt_rand(0, $splitTestIdCount - 1);
    }
    
    private function getSplitTestTestDataForView(){
        
        $pricePlansSubarray = $this -> splitTestDataSubarray['pricePlans'];
        $pricePlansSubarrayValues = array_values($pricePlansSubarray);
        return $pricePlansSubarrayValues[$this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['splitTestId']]];
    }

    private function hasSessionAlreadyHasVisitId(){
        return request() -> session() -> has(REF::SESSION_VARIABLES['visitId']) ? true : false;
    }
    
    private function hasDatabaseVisitRecordWithVisiIdFromSession(){
        
        $recordInDataBase = DB::table(REF::DB_TABLES['landingVisits']['tableName'])
                
                -> where(REF::DB_TABLES['landingVisits']['fieldNames']['id'], '=', 
                    $this -> visitData[REF::SESSION_VARIABLES['visitId']])
                
                -> first();
        
        return is_null($recordInDataBase) ? false: true;
    }
    
    private function areCurrentIpAddressAndUtmDataMatchesWidthVisitRecordInDb(){
        
        $recordInDataBase = DB::table(REF::DB_TABLES['landingVisits']['tableName'])
                
            -> where(REF::DB_TABLES['landingVisits']['fieldNames']['id'], '=', 
                    $this -> visitData[REF::SESSION_VARIABLES['visitId']])
                
            -> where(REF::DB_TABLES['landingVisits']['fieldNames']['ipAddress'], '=', 
                    $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['ipAddress']])
                
            -> where(REF::DB_TABLES['landingVisits']['fieldNames']['utmSource'], '=', 
                    $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmSource']])
                
            -> where(REF::DB_TABLES['landingVisits']['fieldNames']['utmCampaign'], '=', 
                    $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmCampaign']])
                
            -> where(REF::DB_TABLES['landingVisits']['fieldNames']['utmContent'], '=', 
                    $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmContent']])
                
            -> where(REF::DB_TABLES['landingVisits']['fieldNames']['utmTerm'], '=', 
                     $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmTerm']])
                
            -> first();
        
        return is_null($recordInDataBase) ? false: true;
    }
    
    private function getVisitDataFromDatabase(){
        
        $recordInDataBase = DB::table(REF::DB_TABLES['landingVisits']['tableName'])
                
                -> where(REF::DB_TABLES['landingVisits']['fieldNames']['id'], '=', 
                    $this -> visitData[REF::SESSION_VARIABLES['visitId']]) -> first();
        
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['ipAddress']] = request() -> ip();
                
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['visitDate']] = 
                $recordInDataBase -> {REF::DB_TABLES['landingVisits']['fieldNames']['visitDate']};
        
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['sourceReference']] = 
                $recordInDataBase -> {REF::DB_TABLES['landingVisits']['fieldNames']['sourceReference']};
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['splitTestId']] = 
                $recordInDataBase -> {REF::DB_TABLES['landingVisits']['fieldNames']['splitTestId']};
        
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmSource']] = 
                $recordInDataBase -> {REF::DB_TABLES['landingVisits']['fieldNames']['utmSource']};
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmCampaign']] = 
                $recordInDataBase -> {REF::DB_TABLES['landingVisits']['fieldNames']['utmCampaign']};
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmContent']] = 
                $recordInDataBase -> {REF::DB_TABLES['landingVisits']['fieldNames']['utmContent']};
        $this -> visitData[REF::DB_TABLES['landingVisits']['fieldNames']['utmTerm']] = 
                $recordInDataBase -> {REF::DB_TABLES['landingVisits']['fieldNames']['utmTerm']};
    }
}