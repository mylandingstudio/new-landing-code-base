<?php

namespace App\Extenders;

use Illuminate\Support\Facades\Log;

class Notifier {
    
    public static function notifyThatUrlParametersChangedDuringSameVisit(){
        Log::info('Пользователь с IP ... пришел с другими параметрами.');
    }
    
    public static function notifyThatVisitIdExistsInSessionButDoesNotExistInDatabase(){
        Log::info('Visit ID существует в сессии, но соответствующей записи в БД нет.');
    }
    
    public static function notifyAboutAttemptToSendFeedbackAgain(){
        Log::info('Пользователь с ID визита таким-то попытался отправить запос на обратную связь ещё раз.');
    }
    
    public static function notifyAboutIllegalAttemptToRequestThanksPage(){
        Log::info('Пользователь с таким-то ID попытался попасть на страницу благодарности');
    }
}